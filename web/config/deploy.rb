# config valid for current version and patch releases of Capistrano


lock "~> 3.16.0"

set :application, "web"
set :repo_url, "git@gitlab.com:ait-wae-2021/web1/web1-app"
set :rbenv_type, :user
set :rbenv_ruby, '3.0.2'
set :repo_tree, 'web'
set :branch, ENV['BRANCH'] ? ENV['BRANCH'] : 'main'

append :linked_files, "config/database.yml", "config/master.key"

append :linked_dirs, "log", "tmp", "public/system", "public/assets", "public/packs", ".bundle"

set :keep_releases, 5

after 'deploy:publishing', 'puma:restart'
