json.extract! friend, :id, :name, :email, :phoneno, :created_at, :updated_at
json.url friend_url(friend, format: :json)
