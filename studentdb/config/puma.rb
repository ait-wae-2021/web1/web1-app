rails_env = ENV.fetch('Rails_ENV', 'development')
environment rails_env

if rails_env == 'production'
  ENV['https_proxy'] = 'http://192.41.170.23:3128'
  ENV['http_proxy'] = 'http://192/41/170.23:3128'
  workers 2
  preload_app!
  #Location of the control socket. Must be created in advance
  app_dir = File.expand_path('..', __dir__)  
  bind "unix://#{app_dir}/tmp/sockets/puma.sock"
end

threads 1,5



port ENV.fetch("PORT") { 3000 }

# Specifies the `environment` that Puma will run in.
#
#environment ENV.fetch("RAILS_ENV") { "development" }

# Specifies the `pidfile` that Puma will use.
pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }


# Allow puma to be restarted by `rails restart` command.
plugin :tmp_restart

activate_control_app
