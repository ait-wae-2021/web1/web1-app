# Puma can serve each request in a thread from an internal thread pool.
# The `threads` method setting takes two numbers: a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma. Default is set to 5 threads for minimum
# and maximum; this matches the default thread size of Active Recor



rails_env = ENV.fetch("RAILS_ENV") { "development" }
environment rails_env

# If production mode, use two workers with preloaded application

if rails_env == "production"
  ENV['https_proxy'] = 'http://192.41.170.23:3128'
  ENV['http_proxy'] = 'http://192.41.170.23:3128'
  workers 2
  preload_app!
  state_path "tmp/sockets/puma.state"
  # Location of the control socket. Must be created in advance.
  app_dir = File.expand_path("../..", __FILE__)
  bind "unix://#{app_dir}/tmp/sockets/puma.sock"
  directory '/home/deploy/webb/current'
end

# 1-5 threads per worker

threads 1, 5

# Run on port 3000 by default

port ENV.fetch("PORT") { 3000 }

# Specify the server's PID file

pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }

# Allow puma to be restarted by the "rails restart" command

plugin :tmp_restart

# Activate the control app

activate_control_app



# Specifies the number of `workers` to boot in clustered mode.
# Workers are forked web server processes. If using threads and workers together
# the concurrency of the application would be max `threads` * `workers`.
# Workers do not work on JRuby or Windows (both of which do not support
# processes).
#
# workers ENV.fetch("WEB_CONCURRENCY") { 2 }

# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory.
#
# preload_app!

# Allow puma to be restarted by `rails restart` command.

